const express = require('express')

const app = express();
const port = 3000;
app.use(express.json())

app.get('/', (req, res) => {
  res.sendStatus(200)
})

app.post('/', (req, res) => {
  const data = req.body;
  console.log(data)
  res.send(reorganizeMenu(data))
 // res.sendStatus(200)
})

app.listen(port, () => {
  console.log("started")
})

function reorganizeMenu(menu) {
  menu.forEach(categoria => {
    categoria.subcategoria.forEach(subcategoria => {
      subcategoria.produtos.sort(sortByScore)
    });
  });
  return menu
}

function sortByScore(a, b) {
  let max = {
    price: Math.max(a.price, b.price),
    nr_clicks: Math.max(a.nr_clicks, b.nr_clicks),
    nr_orders: Math.max(a.nr_orders, b.nr_orders)
  }
  let aScore = calculateScore(a, max)
  let bScore = calculateScore(b, max)
  if (aScore < bScore) return 1
  if (aScore > bScore) return -1
  return 0;
}

function calculateScore(product, max) {
  const priceWeight = 0.1 * (1 - (product.price + 1 / max.price + 1))
  const clicksWeight = 0.6 * (product.nr_clicks + 1 / max.nr_clicks + 1)
  const ordersWeight = 0.3 * (product.nr_orders + 1 / max.nr_orders + 1)
  return priceWeight + clicksWeight + ordersWeight
}

